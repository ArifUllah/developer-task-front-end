# Static Page Build #

### Task ###

To create a static, responsive page based on supplied designs.

### Details ###

All design files, PSD's and assets can be downloaded from this repository.

Hex values for the colours used are:

* Dark green: #084e37
* Light green: #158842
* Yellow: #fdc02c
* Light grey background: #f5f5f5
* Text colour: #666666

Fonts:
These are provided in the fonts directory.

### Requirements ###

* Complete the task using HTML5 and CSS - or any CSS Pre-Processor of your choice.
* We would like to see you create your own grid rather than relying on a CSS framework.
* Should support IE9+.
* The carousel must be in CSS only - no JavaScript.


### What we're looking for ###

* Semantic mark-up
* Easy to read and understand by novice developers
* Bonus - If you use a CSS architecture

If using a module bundler or task runner, please provide details of dependencies or how to view the page / application.